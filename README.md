# NCNN适配鲲鹏开源验证

NCNN 是由腾讯开源的高性能神经网络推理框架，专为移动端和嵌入式设备设计，具有轻量化、高效和跨平台的特点。本项目致力于实现NCNN适配华为鲲鹏处理器和EulerOS操作系统上的推理部署演示。

鲲鹏、OpenEuler验证:

第一步，在github上拉取相关项目。

git clone https://github.com/Tencent/ncnn.git

cd ncnn

git submodule update --init

第二步，安装依赖环境，编译NCNN

     sudo dnf update
    
     sudo dnf groupinstall "Development Tools"
    
     sudo dnf install cmake protobuf-devel opencv-devel
    
  如果命令安装失败，可以尝试源码编译安装。
  
  编译NCNN命令
  
     cd ncnn 
     mkdir -p build && cd build
     cmake .. -DNCNN_BENCHMARK=ON -DNCNN_VULKAN=OFF -DNCNN_BUILD_TOOLS=OFF -DNCNN_BUILD_EXAMPLES=ON
     make
     make install 
     
   ![11.jpg](https://raw.gitcode.com/pan123/ncnn/attachment/uploads/7a236ffe-cc3b-481f-b67c-0053c3e718bc/11.jpg '11.jpg')
     
第三步，运行程序

执行推理程序前，需要下载模型文件和测试文件。

模型文件下载地址：

    https://github.com/nihui/ncnn-assets/tree/master/models
    
以yolov8为例，下载yolov8n.bin，yolov8n.param两个文件。

测试文件，使用YOLOV8的bus.jpg，zidane.jpg。

执行命令
   
    cd examples
    
    ./yolov8 ./images/bus.jpg
    
运行结果

yolov8推理结果

![999.jpg](https://raw.gitcode.com/pan123/ncnn/attachment/uploads/ee5e9610-339e-4f27-8825-cb3c82023df9/999.jpg '999.jpg')


squeezenet推理结果，打印出每个算子耗时

![33.jpg](https://raw.gitcode.com/pan123/ncnn/attachment/uploads/c43fcf6c-cc90-4b07-9bf2-e9e51049fe84/33.jpg '33.jpg')


scrfd检测结果，检测到人脸
![scrfd_res.jpg](https://raw.gitcode.com/pan123/ncnn/attachment/uploads/449870a3-c12e-4ca1-9d3a-d1e0aa1d2f9d/scrfd_res.jpg 'scrfd_res.jpg')

retinaface检测结果，检测到人脸，还有5个人脸关键点信息
![retina_res.jpg](https://raw.gitcode.com/pan123/ncnn/attachment/uploads/93912b16-7854-4555-9c65-4c76a516305d/retina_res.jpg 'retina_res.jpg')






